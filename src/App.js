import React, { useEffect, useState } from 'react'
import Providers from '../navigation'

const App = () => {
  return <Providers />
}
export default App