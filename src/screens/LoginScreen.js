import React, { useState } from 'react'
import { Button, Image, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import FormInput from '../../components/FormInput'
import FormButton from '../../components/FormButton'
import SocialButton from '../../components/SocialButton'

const LoginScreen = ({ navigation }) => {
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()

    return (
        <View style={styles.container}>
            <Image
                source={require('../assets/img/login.png')} style={styles.logo} />
            <Text style={styles.text}>React Native</Text>

            <FormInput
                placeholderText="Email"
                iconType="user"
                keyboardType="email-address"
                autoCapitalize="none"
                autoCorrect={false}
                labelValue={email}
                onChangeText={(userEmail) => setEmail(userEmail)} />

            <FormInput
                placeholderText="Password"
                iconType="lock"
                labelValue={password}
                secureTextEntry={true}
                onChangeText={(userPassword) => setPassword(userPassword)} />

            <FormButton
                buttonTitle="Login"
                onPress={() => alert('Sign IN')} />

            <TouchableOpacity style={styles.forgotButton} onPress={() => { }}>
                <Text style={styles.navButtonText}>Forgot Password</Text>
            </TouchableOpacity>

            <SocialButton
                buttonTitle='Sign in with facebook'
                btnType='facebook'
                color='#4867aa'
                backgroundColor='#e6eaf4'
                onPress={() => { }} />

            <SocialButton
                buttonTitle='Sign in with google'
                btnType='google'
                color='#de4d41'
                backgroundColor='#f5e7ea'
                onPress={() => { }} />

            <TouchableOpacity style={styles.forgotButton}
                onPress={() => navigation.navigate('Signup')}>
                <Text style={styles.navButtonText}>Don't have an account? Create here</Text>
            </TouchableOpacity>
        </View >
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,
        backgroundColor: '#f9fafd',
    },
    logo: {
        height: 100,
        width: 250,
        resizeMode: 'cover',

    },
    text: {
        fontFamily: 'kufam-SemiBoldItalic',
        fontSize: 28,
        marginBottom: 10,
        color: '#051d5f'
    },
    navButton: {
        marginTop: 15
    },
    forgotButton: {
        marginVertical: 10,
    },
    navButtonText: {
        fontSize: 15,
        fontWeight: '500',
        color: '#2e64e5',
        fontFamily: 'lato-Regular'
    }
})
