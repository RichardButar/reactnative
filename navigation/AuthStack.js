import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import LoginScreen from '../src/screens/LoginScreen'
import OnboardingScreen from '../src/screens/OnboardingScreen'
import SignupScreen from '../src/screens/SignupScreen'

import AsyncStorage from '@react-native-community/async-storage'

import FontAwesome from 'react-native-vector-icons/FontAwesome'

const Stack = createStackNavigator()

const AuthStack = () => {

    const [isFirstLaunch, setIsFirstLaunch] = React.useState(null)
    let routeName;

    useEffect(() => {
        AsyncStorage.getItem('alreadyLaunch').then(value => {
            if (value == null) {
                AsyncStorage.setItem('alreadyLaunch', 'true')
                setIsFirstLaunch(true)
            } else {
                setIsFirstLaunch(false)
            }
        })
    }, [])

    if (isFirstLaunch === null) {
        return null
    } else if (isFirstLaunch == true) {
        routeName = 'Onboarding'
    } else {
        routeName = 'login'
    }

    return (
        <Stack.Navigator initialRouteName={routeName}>
            <Stack.Screen
                name="Onboarding"
                component={OnboardingScreen}
                options={{ header: () => null }} />
            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{ header: () => null }} />
            <Stack.Screen
                name="Signup"
                component={SignupScreen}
                options={({ navigation }) => ({
                    title: '',
                    headerStyle: {
                        backgroundColor: '#f9fafd',
                        shadowColor: '#f9fafd',
                        elevation: 0,
                    },
                    headerLeft: () => (
                        <View style={{ marginLeft: 10 }}>
                            <FontAwesome.Button
                                name="long-arrow-left"
                                size={25}
                                backgroundColor='#f9fafd'
                                color='#333'
                                onPress={() => navigation.navigate('Login')} />
                        </View>
                    )
                })} />
        </Stack.Navigator>
    )
}

export default AuthStack

